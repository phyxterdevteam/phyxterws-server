﻿using System.Web.Mvc;

namespace PhyxterWS.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}